package com.example.examen.database;

import java.io.Serializable;

public class persistencia implements Serializable {
    private long codigo;
    private String nombre;
    private String marca;
    private int precio;
    private String perecedero;

    public persistencia(long codigo, String nombre, String marca, int precio, String perecedero) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.marca = marca;
        this.precio = precio;
        this.perecedero = perecedero;
    }

    public long getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getMarca() {
        return marca;
    }

    public int getPrecio() {
        return precio;
    }

    public String getPerecedero() {
        return perecedero;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public void setPerecedero(String perecedero) {
        this.perecedero = perecedero;
    }
}
