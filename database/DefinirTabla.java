package com.example.examen.database;

import android.provider.BaseColumns;

public class DefinirTabla {
    public DefinirTabla() {
    }

    public static abstract class datos implements BaseColumns {
        public static final String TABLA_NAME= "datos";
        public static final String CODIGO= "codigo";
        public static final String NOMBRE= "nombre_producto";
        public static final String MARCA= "marca";
        public static final Integer PRECIO=0;
        public static final String PEREDECERO= "perecedero";

    }
}
