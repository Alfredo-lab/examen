package com.example.examen.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class baseDatos extends SQLiteOpenHelper {
    private static final String TEXT_TYPE= " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA = " ,";
    private static final String SQL_CREATE_DATOS = " CREATE TABLE " +
            DefinirTabla.datos.TABLA_NAME + " (" +
            DefinirTabla.datos.CODIGO + " INTEGER PRIMARY KEY, " +
            DefinirTabla.datos.NOMBRE + TEXT_TYPE + COMMA +
            DefinirTabla.datos.MARCA + TEXT_TYPE + COMMA +
            DefinirTabla.datos.PRECIO + INTEGER_TYPE + COMMA +
            DefinirTabla.datos.PEREDECERO + TEXT_TYPE + ")";

    private static final String SQL_DELETE_DATOS = "DROP TABLE IF EXISTS " + DefinirTabla.datos.TABLA_NAME;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "datos.db";

    public baseDatos(@Nullable Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_DATOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_DATOS);
        onCreate(db);
    }
}
